package com.example.practicaservicio.api.Servicios;

import com.example.practicaservicio.ViewModels.Mostrar_Usuarios;
import com.example.practicaservicio.ViewModels.Peticion_Login;
import com.example.practicaservicio.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {

    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);


    @POST("api/todosUsuarios")
    Call<Mostrar_Usuarios> getUsuariosP();

}
