package com.example.practicaservicio;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicaservicio.ViewModels.Mostrar_Usuarios;
import com.example.practicaservicio.api.Api;
import com.example.practicaservicio.api.Servicios.ServicioPeticion;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Usuarios extends AppCompatActivity {

    ArrayList<String> C_Usuarios = new ArrayList<>();
    ArrayList<String> C_Id = new ArrayList<>();
    ArrayList<String> C_Password = new ArrayList<>();
    ArrayList<String> C_Created = new ArrayList<>();
    ArrayList<String> C_Updated = new ArrayList<>();

    public ListView listaUsuarios;
    public TextView TVid, TVusuario, TVpassword, TVcerrar, TVcreated, TVupdated;

    public String Detalle[];
    public String D_Pass[];
    public String D_Created[];
    public String D_Updated[];

    public Button principal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuarios);

        getSupportActionBar().hide();

        //_______________________________________________________

        TVusuario = (TextView)findViewById(R.id.tv_usuario);
        TVid = (TextView)findViewById(R.id.tv_id);
        TVpassword = (TextView)findViewById(R.id.tv_pass);
        TVcreated = (TextView)findViewById(R.id.tv_created);
        TVupdated = (TextView)findViewById(R.id.tv_updated);
        listaUsuarios = (ListView)findViewById(R.id.list);
        TVcerrar = (TextView)findViewById(R.id.tv_cerrar);
        principal= (Button)findViewById(R.id.btn_principal);

        principal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent1 = new Intent(Usuarios.this, Principal.class);
                startActivity(intent1);
            }
        });

        //_______________________________________________________


        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,C_Usuarios);

        listaUsuarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TVusuario.setText("Usuario: " + listaUsuarios.getItemAtPosition(position));
                TVid.setText("ID: " + Detalle[position]);
                TVpassword.setText("Pass: " + D_Pass[position]);
                TVcreated.setText("Created: " + D_Created[position]);
                TVupdated.setText("Updated " + D_Updated[position]);
            }
        });

        //MostrarUsers();

        //___________________________________________________________________________________________

        ServicioPeticion service = Api.getApi(Usuarios.this).create(ServicioPeticion.class);
        Call<Mostrar_Usuarios> UsuariosCall = service.getUsuariosP();
        UsuariosCall.enqueue(new Callback<Mostrar_Usuarios>() {
            @Override
            public void onResponse(Call<Mostrar_Usuarios> call, Response<Mostrar_Usuarios> response) {
                Mostrar_Usuarios peticion = response.body();

                if(peticion.estado.equals("true")){

                    List<ClassUsers> Pe_Usuarios = peticion.usuarios;

                    for (ClassUsers mostrar : Pe_Usuarios){
                        C_Usuarios.add(mostrar.getUsername());
                    }
                    listaUsuarios.setAdapter(arrayAdapter);

                    for (ClassUsers mostrar : Pe_Usuarios){
                        C_Id.add(mostrar.getId());
                    }
                    Detalle = C_Id.toArray(new String[C_Id.size()]);

                    for (ClassUsers mostrar : Pe_Usuarios){
                        C_Password.add(mostrar.getPassword());
                    }
                    D_Pass = C_Password.toArray(new String[C_Password.size()]);

                    for (ClassUsers mostrar : Pe_Usuarios){
                        C_Created.add(mostrar.getCreated_at());
                    }
                    D_Created = C_Created.toArray(new String[C_Created.size()]);

                    for (ClassUsers mostrar : Pe_Usuarios){
                        C_Updated.add(mostrar.getUpdated_at());
                    }
                    D_Updated = C_Updated.toArray(new String[C_Updated.size()]);

                }else {

                    Toast.makeText(Usuarios.this,"Error",Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<Mostrar_Usuarios> call, Throwable t) {

                Toast.makeText(Usuarios.this,"Error",Toast.LENGTH_LONG).show();

            }
        });

        //___________________________________________________________________________________________________

        /*TVcerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                String token = "";
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("TOKEN", token);
                editor.commit();

                Intent intent1 = new Intent(Usuarios.this, MainActivity.class);
                startActivity(intent1);
            }
        });*/

        //____________________________________________________________________________________________________


    }//onCreate

    /*public void MostrarUsers(){

        ServicioPeticion service = Api.getApi(Usuarios.this).create(ServicioPeticion.class);
        Call<Mostrar_Usuarios> UsuariosCall = service.getUsuariosP();
        UsuariosCall.enqueue(new Callback<Mostrar_Usuarios>() {
            @Override
            public void onResponse(Call<Mostrar_Usuarios> call, Response<Mostrar_Usuarios> response) {
                Mostrar_Usuarios peticion = response.body();

                if(peticion.estado.equals("true")){

                    List<ClassUsers> Pe_Usuarios = peticion.usuarios;

                    for (ClassUsers mostrar : Pe_Usuarios){
                        C_Usuarios.add(mostrar.getUsername());
                    }
                    listaUsuarios.setAdapter(arrayAdapter);

                    for (ClassUsers mostrar : Pe_Usuarios){
                        C_Id.add(mostrar.getId());
                    }
                    Detalle = C_Id.toArray(new String[C_Id.size()]);

                    for (ClassUsers mostrar : Pe_Usuarios){
                        C_Password.add(mostrar.getPassword());
                    }
                    D_Pass = C_Password.toArray(new String[C_Password.size()]);

                    for (ClassUsers mostrar : Pe_Usuarios){
                        C_Created.add(mostrar.getCreated_at());
                    }
                    D_Created = C_Created.toArray(new String[C_Created.size()]);

                }else {

                    Toast.makeText(Usuarios.this,"Error",Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<Mostrar_Usuarios> call, Throwable t) {

                Toast.makeText(Usuarios.this,"Error",Toast.LENGTH_LONG).show();

            }
        });METODO

    }*/
}//Usuarios
