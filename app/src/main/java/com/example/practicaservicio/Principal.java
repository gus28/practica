package com.example.practicaservicio;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Principal extends AppCompatActivity {

    private Button general, especifico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        general = (Button)findViewById(R.id.btn_general);
        especifico = (Button)findViewById(R.id.btn_especifico);

        FirebaseMessaging.getInstance().subscribeToTopic("atodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(Principal.this,"Se agrego a la lista", Toast.LENGTH_SHORT).show();
            }
        });

        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mostrarGeneral();

            }
        });

        especifico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mostrarEspecifico();

            }
        });
    }

    public void mostrarGeneral(){

        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();

        try {

            //String token = "eMxcECEmTGqUH66BA0KqYm:APA91bF4LXmWrGZQ9BumYFmxDxgYEb7In7M2jwF5bYMwUwboZBg42zhlHoTKlMvTKc-UTTRzUv15yUlvDKWKqp3FK5YM1F1HAgTs95GPhWvVGdMK9of_VCAJj0AAmJCh7_0Cm4HMUq6d";
            json.put("to", "/topics/"+"atodos");
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo", "Mostar titulo");
            notificacion.put("detalle", "Mostrando contenido");
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json, null, null){
                @Override
                public Map<String, String> getHeaders() {
                    Map<String,String> header = new HashMap<>();
                    header.put("content-type", "application/json");
                    header.put("authorization", "key=AAAAZlcVZL8:APA91bFRsFFg6ejGAAm6ldgfl4KSPED7Jfz5dYREiUh5r7WqVoOFRBS_KlGeUZL4j7xaBnyOt3KcfNTKZ6h7m2RoqJQMmgQXfThiNAZBexx9Zd_fs5AJJ5rvSB2RAq0y2SNGzAO-4hUl");
                    return header;
                }
            };

        }catch (JSONException e){

            e.printStackTrace();
        }

    }

    public void mostrarEspecifico(){

        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();

        try {

            String token = "eMxcECEmTGqUH66BA0KqYm:APA91bF4LXmWrGZQ9BumYFmxDxgYEb7In7M2jwF5bYMwUwboZBg42zhlHoTKlMvTKc-UTTRzUv15yUlvDKWKqp3FK5YM1F1HAgTs95GPhWvVGdMK9of_VCAJj0AAmJCh7_0Cm4HMUq6d";
            json.put("to", token);
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo", "Mostar titulo");
            notificacion.put("detalle", "Mostrando contenido");
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json, null, null){
                @Override
                public Map<String, String> getHeaders() {
                    Map<String,String> header = new HashMap<>();
                    header.put("content-type", "application/json");
                    header.put("authorization", "key=AAAAZlcVZL8:APA91bFRsFFg6ejGAAm6ldgfl4KSPED7Jfz5dYREiUh5r7WqVoOFRBS_KlGeUZL4j7xaBnyOt3KcfNTKZ6h7m2RoqJQMmgQXfThiNAZBexx9Zd_fs5AJJ5rvSB2RAq0y2SNGzAO-4hUl");
                    return header;
                }
            };

        }catch (JSONException e){

            e.printStackTrace();
        }

    }
}