package com.example.practicaservicio.ViewModels;

import com.example.practicaservicio.ClassUsers;

import java.util.ArrayList;

public class Mostrar_Usuarios {


    public String estado;
    public String detalle;
    public ArrayList<ClassUsers> usuarios;

    public Mostrar_Usuarios(){

    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public ArrayList<ClassUsers> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList<ClassUsers> usuarios) {
        this.usuarios = usuarios;
    }
}
